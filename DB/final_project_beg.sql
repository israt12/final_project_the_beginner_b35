-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2016 at 09:56 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `final_project_beg`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'jerinisrat@gmail.com', '1234'),
(2, 'israt@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
`id` int(11) NOT NULL,
  `patient_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `catagory` varchar(111) NOT NULL,
  `doctor_name` varchar(111) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `patient_name`, `email`, `catagory`, `doctor_name`, `date`) VALUES
(6, 'zvzz', '', 'Surgeons', 'Professor Dr. A.K.M. Anwarul Islam', '0000-00-00'),
(7, 'ema', '', 'Cardiologist', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(8, 'hhh', 'jerinisrat.cse@gmail.com', 'Cardiologist', 'Professor Dr. A. B. M. Yunus', '0000-00-00'),
(9, 'hhh', 'jerinisrat.cse@gmail.com', 'Bone', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(10, 'hhh', 'israt.ctg75@gmail.com', 'Dentist', 'Professor Dr. A. B. M. Yunus', '0000-00-00'),
(11, 'vcb', 'jerinisrat.cse@gmail.com', 'Bone', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(12, 'jjjj', 'jerinisrat.cse@gmail.com', 'Cardiologist', 'Professor Dr. A.A. Quoreshi', '0000-00-00'),
(13, 'anu', 'abc@gmail.com', 'Cardiologist', 'Professor Dr. A. B. M. Yunus', '2016-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
`id` int(11) NOT NULL,
  `doctor_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `catarory` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(111) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `doctor_name`, `email`, `password`, `catarory`, `phone`, `address`) VALUES
(2, 'ema', 'jerinisrat32@gmail.com', '1234566', 'zvzxcxcnn', '80854666', 'vhnhh'),
(3, 'abu nasir', 'asd@gmail.com', '12365', 'eyespecialist', '015656565', 'ctg'),
(4, 'abul kuddus', 'asr@gmail.com', '1234', 'asd', '123645', 'ert');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(111) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(1, 'Israt', 'ema', 'israt.ctg75@gmail.com', '182be0c5cdcd5072bb1864cdee4d3d6e', '33', 'cc', 'd87391f56ebb944ce0d45cd13b7aec96'),
(2, 'Tasnia', 'xc', 'jerinisrat.cse@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '436', 'dgffdg', 'Yes'),
(3, 'test1', 'test2', 'shwetariya1611@gmail.com', '202cb962ac59075b964b07152d234b70', '3647588', 'fhgg43654 3646 ', '8ff56f513c85467703e9c61be0a24f2b'),
(4, 'test1', 'hhg', 'graphical4200@gmail.com', '202cb962ac59075b964b07152d234b70', '677', 'jhjhhj', 'Yes'),
(8, 'gggg', 'gg', 'milondas117@gmail.com', '202cb962ac59075b964b07152d234b70', '2555', 'kkkk', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
