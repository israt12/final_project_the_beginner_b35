<?php
include_once('../../../../vendor/autoload.php');
use App\Booking\Booking;

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>booking</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets2/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets2/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets2/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets2/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets2/js/html5shiv.js"></script>
    <script src="../../../resource/assets2/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Top content -->
<div class="top-content" >
    <div class="container">
        <a href='../index.php' style="margin-left: 900px;"><button class='btn btn-success'>Back Index page</button></a>
        <div class="row" style="margin-top: -55px">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>New Booking</h3>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="Booking/store.php" method="post" class="login-form">
                        <div class="form-group">
                            <label class="" for="name">Patient Name :</label>
                            <input type="text" name="patient_name" placeholder="patient name..." class="form-name form-control" id="form-name">
                        </div>
                        <div class="form-group">
                            <label class="" for="email" >Email:</label>
                            <input type="email" name="email" placeholder="email..." class="form-control" id="">

                        </div>
                        <div class="form-group">
                            <label class="" for="catagory">Catagory</label>
                            <select name="catagory" class="form-control">
                                <option   value="">..Select Catagory..</option>
                                <option   value="Dentist">Dentist</option>
                                <option  value="Cardiologist">Cardiologist</option>
                                <option  value="Bone">Bone</option>
                                <option  value="Heart">Heart</option>
                                <option  value="Surgeons">Surgeons</option>
                                <option  value="Psychiatrists">Psychiatrists</option>
                                <option  value="Kidney">Kidney</option>
                                <option  value="General Physician">General Physician</option>
                                <option  value="Gynecologist">Gynecologist</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="" for="doctor">Doctor</label>
                            <select name="doctor_name" class="form-control">
                                <option   value="">..Select Doctor..</option>
                                <option   value="Professor Dr. A. B. M. Yunus">Professor Dr. A. B. M. Yunus</option>
                                <option  value="Professor Dr. A.A. Quoreshi">Professor Dr. A.A. Quoreshi</option>
                                <option  value="Professor Dr. A.K.M. Anwarul Islam">Professor Dr. A.K.M. Anwarul Islam</option>
                                <option  value="Professor Dr. ( Brig. Gen ) Md. Saidur Rahman">Professor Dr. ( Brig. Gen ) Md. Saidur Rahman</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="" for="date" >Date :</label>
                            <input type="date" name="date" placeholder="date..." class="form-date form-control" id="date">

                        </div>

                        <button type="submit" class="btn">Book !</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets2/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets2/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets2/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets2/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets2/js/placeholder.js"></script>
<![endif]-->
</body>

</html>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>