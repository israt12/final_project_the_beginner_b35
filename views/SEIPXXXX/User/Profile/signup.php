<?php
include_once('../../../../vendor/autoload.php');

if(!isset($_SESSION) )session_start();
use App\Message\Message;


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Registration Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Oleo+Script:400,700'>
    <link rel="stylesheet" href="../../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../resource/assets/css/style.css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>

<div class="header">
    <div class="container">

        <div class="row">
            <div class="logo span4">
                <h1><a href=""> Register <span class="red">.</span></a></h1>
            </div>
            <div class="links span8">
                <a class="home" href="login_page.php" rel="tooltip" data-placement="bottom" data-original-title="Home"></a>
            </div>
        </div>
    </div>
</div>
<table>
    <tr>
        <td width='430' >

        <td width='500' style="margin-left: 400px">


            <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                <div  id="message" class="form button"   style="font-size: smaller  " >
                    <center>
                        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                            echo "&nbsp;".Message::message();
                        }
                        Message::message(NULL);
                        ?></center>
                </div>
            <?php } ?>
        </td>
    </tr>
</table>

<div class="register-container container">
    <div class="row" style="margin-left: 280px">
        <div class="register span6">
            <form role="form" action="registration.php" method="post" class="registration-form">
                <h2>REGISTER  <span class="red"></span></h2>
                <p>It's free and always will be</p>
                <div class="form-group">
                    <label class="sr-only" for="form-first_name">First name</label>
                    <input type="text" name="first_name" placeholder="First name..." class="form-first-name form-control" id="form-first-name">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-last-name">Last name</label>
                    <input type="text" name="last_name" placeholder="Last name..." class="form-last-name form-control" id="form-last-name">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-email">Email</label>
                    <input type="text" name="email" placeholder="Email..." class="form-email form-control" id="form-email">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="form-password">Password</label>
                    <input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-email">Phone</label>
                    <input type="text" name="phone" placeholder="Phone..." class="form-phone form-control" id="form-phone">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="address">Address</label>
                    <input type="text" name="address" placeholder="Address..." class="form-address form-control" id="form-address">
                </div>
                <button type="submit">REGISTER</button>
            </form>
        </div>
    </div>
</div>

<!-- Javascript -->
<script src="../../../../resource/assets/js/jquery-1.8.2.min.js"></script>
<script src="../../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../../resource/assets/js/scripts.js"></script>

</body>

</html>
<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>

