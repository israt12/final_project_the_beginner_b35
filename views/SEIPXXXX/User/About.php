<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Appointment management system</title>


    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/assets3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../../resource/assets3/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom Theme files -->
    <link href="../../../resource/assets3/css/style.css" rel='stylesheet' type='text/css' />

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../../resource/assets3/js/jquery.min.js"></script>

    <!-- Owl Carousel Assets -->
    <link href="../../../resource/assets3/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="../../../resource/assets3/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets3/js/html5shiv.js"></script>
    <script src="../../../resource/assets3/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- /////////////////////////////////////////Top -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="logo"><img src="../../../resource/assets3/images/logo.png" alt="Third slide"></a>  <!... logo of health care...!>
            </div>
            <div class="col-md-6 text-right">
                <span>Information Service:</span></br>
                <strong class="contact-phone"><i class="fa fa-phone"></i>0181xxxxx</strong>
            </div>
        </div>
    </div>
</header>
<!-- Header -->


<!-- /////////////////////////////////////////Navigation -->
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="dropdown-toggle"   href="index.php">Home</a>
                </li>
                <li>
                    <a class="dropdown-toggle"   href="About.php">About</a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="Doctor.php">Doctors Info
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="Doctor.php">services</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">medicine</a></li>
                    </ul>
                </li>
                <li>
                    <a class="dropdown-toggle"  href="Blog.php">Blogs</a>
                </li>
                <li>
                    <a class="dropdown-toggle"  href="phone.php">Contacts</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<a id='backTop'>Back To Top</a>
<!-- /Back To Top -->

<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="single-page">
    <div class="container">
        <div id="main-content">
            <div class="row">
                <article class="box-shadow">
                    <div class="art-header">
                        <h2>Sharing Your Explorer’s Story: Man and Mother Nature</h2>
                        <div class="info">By <a href="#">Danny</a> on April 14, 2015</div>
                    </div>
                    <div class="art-content">
                        <img src="../../../resource/assets3/images/7.jpg" />
                        <div class="excerpt"><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum exercitation ullamco laboris nisi ut aliquip.</p></div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                            sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                        <blockquote><p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet vultatup duista.</p></blockquote>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis
                            at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril.</p>
                        <h2>Heading 1</h2>
                        <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse lorem ipsum dolor sit amet.</p>
                        <h2>Heading 2</h2>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.
                            At vero eos et accusam et justo.</p>
                        <h2>Heading 3</h2>
                        <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            At vero eos et accusam et justo duo dolores et ea rebum hendrerit in vulputate velit esse molestie.</p>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                            sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>

                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                            sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                        <div class="note">
                            <ol>
                                <li>Lorem ipsum</li>
                                <li>Sit amet vultatup nonumy</li>
                                <li>Duista sed diam</li>
                            </ol>
                            <div class="clear"></div>
                        </div>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at
                            vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                        <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores.</p>
                    </div>
                </article>
            </div>
        </div>
    </div>

</div>

<footer>
    <div class="wrap-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-footer footer-1">
                    <div class="footer-heading"><h4>Partners</h4></div>
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/22.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/23.jpg" /></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/24.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/18.jpg" /></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/19.jpg" /></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"><img src="../../../resource/assets3/images/20.jpg" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-2">
                    <div class="footer-heading"><h4>About Us</h4></div>
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad</p>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-3">
                    <div class="footer-heading"><h4>Follow us</h4></div>
                    <div class="content">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a></li>
                            <li><a href="#"><i class="fa fa-rss"></i> RSS</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-footer footer-4">
                    <div class="footer-heading"><h4>Navigation</h4></div>
                    <div class="content">
                        <ul>
                            <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="About.php"><i class="fa fa-users"></i> About</a></li>
                            <li><a href="Doctor.php"><i class="fa fa-ambulance"></i> Doctors Info</a></li>
                            <li><a href="Blog.php"><i class="fa fa-folder-open-o"></i>  Blogs</a></li>
                            <li><a href="phone.php"><i class="fa fa-envelope-o"></i> Contacts</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="coppy-right">
    <div class="wrap-footer">
        <div class="clearfix">
            <div class="col-md-6 col-md-offset-3">
                <p>Copyright @ 2016 The Begginners</p>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->

<!-- Core JavaScript Files -->
<script src="../../../resource/assets3/js/bootstrap.min.js"></script>
<script src="../../../resource/assets3/js/jquery.backTop.min.js"></script>
<script>
    $(document).ready( function() {
        $('#backTop').backTop({
            'position' : 1200,
            'speed' : 500,
            'color' : 'red',
        });
    });
</script>

</body>
</html>
	
