<?php
require_once("../../../vendor/autoload.php");

use App\Admin\Admin;

$obj=new Admin();
$obj->setData($_GET);

$oneData=$obj->view("obj");


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Doctor info</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets1/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets1/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets1/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets1/css/style.css">


    <script src="../../../resource/assets1/js/html5shiv.js"></script>
    <script src="../../../resource/assets1/js/respond.min.js"></script>
    <script type="text/javascript" src="../../../resource/assets1/bootstrap/js/jquery.js"></script>

    <![endif]-->
</head>

<body">

<!-- Top content -->
<div class="top-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Edit Doctor info </h3>
                        <p></p>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="update.php" method="post" class="login-form">
                        <input type="hidden" name="id" value="<?php echo $oneData->id?>">

                        <div class="form-group">
                            <label class="" for="name">Doctor Name :</label>
                            <input type="text" name="doctor_name" value="<?php echo $oneData->doctor_name?>" class="form-name form-control" id="form-doctor_name">
                        </div>
                        <div class="form-group">
                            <label class="" for="email" >Email:</label>
                            <input type="email" name="email" value="<?php echo $oneData->email?>" class="form-control" id="">

                        </div>

                        <div class="form-group">
                            <label class="" for="name">Password:</label>
                            <input type="password" name="password" value="<?php echo $oneData->password?>" class="form-name form-control" id="form-password">
                        </div>

                        <div class="form-group">
                            <label class="" for="name">Catagory :</label>
                            <input type="text" name="catarory" value="<?php echo $oneData->catarory?>" class="form-name form-control" id="form-catarory">
                        </div>

                        <div class="form-group">
                            <label class="" for="name">Phone :</label>
                            <input type="text" name="phone" value="<?php echo $oneData->phone?>" class="form-name form-control" id="form-phone">
                        </div>

                        <div class="form-group">
                            <label class="" for="address">Address:</label>
                            <input type="text" name="address" value="<?php echo $oneData->address?>" class="form-name form-control" id="form-address">
                        </div>
                        <button type="submit" class="btn">Update!</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets1/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets1/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets1/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets1/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets1/js/placeholder.js"></script>
<![endif]-->
</body>

</html>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>