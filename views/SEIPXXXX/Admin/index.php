<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
    <img src="../../../resource/assets/img/backgrounds/table.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 100%; height: 100%;  z-index: -999999; top: 0px;">


    <link rel="stylesheet" href="../../../resource/assets1/bootstrap/css/jquery-ui.css">
    <script src="../../../resource/assets1/bootstrap/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/assets1/bootstrap/js/jquery-ui.js"></script>

    <style>
        table{
            text-align: center;
            margin :50px auto;
            background-color:rgba(183, 179, 179, 0.15);
        }
        th{
            text-align: center;
            height: 32px;
            color: #5e90c3;
            font-family: serif;
            font-size: 19px;
        }
        td,tr{
            padding: 20px;
            text-align: center;
        }
    </style>
    <script language="JavaScript" type="text/javascript">
        function checkDelete()
        {
            return confirm("Are you sure you want to delete");
        }
    </script>
</head>
<?php
require_once("../../../vendor/autoload.php");

use App\Admin\Admin;
use App\Utility\Utility;

$obj=new Admin();

$allData=$obj->index("obj");

################## search  block1 start ##################
if(isset($_REQUEST['search']) )$allData =  $obj->search($_REQUEST);

$availableKeywords=$obj->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block1 end ##################

################## search  block2 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $obj->search($_REQUEST);
    $serial = 1;
}
################## search  block2 end ##################


?>
<a href='create.php' style="margin-left: 1190px;"><button class='btn btn-success'>Add New</button></a>
<form id="searchForm" action="index.php"  method="get" style="margin-left:90px">
    <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" ><br>
    <input type="checkbox"  name="byName"   checked  >By Name
    <input type="checkbox"  name="byCatagory"  checked >By Catagory
    <input hidden type="submit" class="btn-primary" value="search">
</form>
<div class="m"  style="margin-left: 850px">
    <td>
        <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
        <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
    </td>
</div>

<?php
$serial=1;

echo "<table width='1200px'>";
echo "<th> Serial No</th><th > ID</th><th>Doctor Name</th><th>Email</th><th>Password</th><th>Catagory</th><th>Phone</th><th>Address</th><th>Action </thth>";
foreach($allData as $oneData)

{
    echo "<tr style='height: 40px'>";
    echo "<td> $serial</td>";
    echo "<td> $oneData->id</td>";
    echo "<td> $oneData->doctor_name</td>";
    echo "<td> $oneData->email</td>";
    echo "<td> $oneData->password</td>";
    echo "<td> $oneData->catarory</td>";
    echo "<td> $oneData->phone</td>";
    echo "<td> $oneData->address</td>";
    echo"
  <td>
        <a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a>
        <a href='edit.php?id=$oneData->id'><button class='btn btn-success'>Edit</button></a>
        <a href='delete.php?id=$oneData->id'onclick='return checkDelete()'><button class='btn btn-danger'><img src='../../../resource/assets1/img/backgrounds/del.png' width='15' height='15'> Delete</button></a>
        <a href='email.php?id=$oneData->id'><button class='btn btn-info'>Email</button></a>

   </td>
  ";

    echo "</tr>";
    $serial++;
}
echo "</table>";
?>
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
